import { Router } from "express";

import {userLogin} from '../controllers/login';

class login{
    router = Router();
    uLogin = new userLogin();

    constructor(){
        this.loginUser();
    }

    loginUser(){
        this.router.route("/login").post(this.uLogin.login);
    }
}

export default new login().router;