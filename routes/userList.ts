import { Router } from "express";
import { userList } from "../controllers/userList";

class uList{
    router = Router();
    userDetail = new userList();

    constructor(){
        this.findAll();
        this.findOne();
    }

    findAll(){
        this.router.route('/userList').get(this.userDetail.list);
    }

    findOne(){
        this.router.route('/userlist/:id').get(this.userDetail.findOne);
    }
}

export default new uList().router;