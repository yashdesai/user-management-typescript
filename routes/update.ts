import { Router } from "express";
import { updateDetails } from "../controllers/update";

class uUpdate {
  router = Router();
  uDetails = new updateDetails();

  constructor() {
    this.updateRoute();
  }

  updateRoute() {
    this.router.route("/update/:id").patch(this.uDetails.updateUser);
  }
}

export default new uUpdate().router;
