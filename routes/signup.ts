import { Router } from "express";
import {newUser} from "../controllers/signup";

class user {
    router = Router();
    userReg = new newUser();

    constructor(){
        this.signUser();
    }

    signUser(){
        this.router.route("/signup").post(this.userReg.User)
    }
}

export default new user().router