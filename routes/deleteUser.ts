import { Router } from "express";
import { remove } from "../controllers/deleteUser";

class deleteUser {
  router = Router();
  removeUser = new remove();

  constructor() {
    this.deleteRoute();
  }

  deleteRoute() {
    this.router.route("/delete/:id").patch(this.removeUser.deleteUser);
  }
}

export default new deleteUser().router;
