import { Request, Response } from "express";
import { User } from "../models/user";

class updateDetails {
  updateUser = async (req: Request, res: Response) => {
    try {
      const user: any = await User.findById(req.params.id);
      (user.email = req.body.email),
        (user.name = req.body.name),
        (user.address = req.body.address),
        (user.phone = req.body.phone),
        (user.gender = req.body.gender),
        (user.dob = req.body.dob),
        (user.category = req.body.category),
        (user.password = req.body.name + "@sa");

      const u = await user.save();
      res.json(u);
    } catch (err: any) {
      res.send("error");
      console.log(err.message);
    }
  };
}
export { updateDetails };
