import { Request, Response } from "express";
import { User } from "../models/user";

class newUser{
  User = async (req: Request, res: Response) => {
    try {
      let email = req.body.email;
      let enteredEmail = await User.findOne({ email: email });

      if (enteredEmail) {
        return res.json({ message: "User already exists." });
      } else {
        const user = new User({
          email: req.body.email,
          name: req.body.name,
          address: req.body.address,
          phone: req.body.phone,
          gender: req.body.gender,
          dob: req.body.dob,
          category: req.body.category,
          created: new Date().toString(),
          password: req.body.name + "@sa",
        });

        const u1 = await user.save();
        res.json({ message: "Registration successful." });
      }
    } catch (err) {
      return res.json({ message: "Registration failed." });
    }
  };
}

export { newUser };
