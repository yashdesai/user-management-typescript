import { Request, Response } from "express";
import { User } from "../models/user";

class remove {
  deleteUser = async (req: Request, res: Response) => {
    try {
      const user: any = await User.findById(req.params.id);
      const u = await user.remove();
      res.json({ message: "User deleted" });
    } catch (err) {
      res.send("eror deleting user");
    }
  };
}

export { remove };
