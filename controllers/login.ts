import { Request, Response } from "express";
import { User } from "../models/user";

class userLogin {
  login = async (req: Request, res: Response) => {
    try {
      const email = req.body.email;
      const password = req.body.password;

      const loginEmail: any = await User.findOne({
        email: email,
      });
      if (loginEmail.password === password) {
        console.log("Login success");
        res.json({ message: "Logged in" });
      } else {
        res.json({ message: "Incorrect password" });
      }
    } catch (err) {
      res.json({ message: "Invalid login detail" });
    }
  };
}

export { userLogin };
