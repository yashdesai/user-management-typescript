import { Request, Response } from "express";
import { User } from "../models/user";

class userList {
  //getting all users
  list = async (req: Request, res: Response) => {
    try {
      const users = await User.find();
      res.json(users);
    } catch (err) {
      res.send("Error" + err);
    }
  };

  //getting one user
  findOne = async (req: Request, res: Response) => {
    try {
      const user: any = await User.findById(req.params.id);
      const { password, created, ...others } = user._doc;
      res.send({ ...others });
    } catch (err) {
      res.send("Error" + err);
    }
  };
}

export { userList };
