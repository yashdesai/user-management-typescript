import express from "express";
import { json } from "body-parser";
import mongoose from "mongoose";
import * as dotenv from "dotenv";

dotenv.config();
const url: any = process.env.DATABASE_URL;

import signUp from "../routes/signup";
import logIn from "../routes/login";
import userList from "../routes/userList";
import deleteUser from "../routes/deleteUser";
import updateUser from "../routes/update";

const app = express();
app.use(json());

mongoose.connect(url, () => {
  console.log("connected to database");
});

//for signup
app.use("/", signUp);
//for login
app.use("/", logIn);
//getting user list
app.use("/", userList);
//deleting user with the help of id
app.use("/", deleteUser);
//update user
app.use("/", updateUser);

app.listen(3000, () => {
  console.log("Server is up on port");
});
