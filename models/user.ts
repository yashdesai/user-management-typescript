import { Schema, model } from "mongoose";
import isEmail from "validator/lib/isEmail";

interface IUser {
  email: String;
  name: String;
  address: String;
  phone: Number;
  gender: String;
  dob: String;
  category: String;
  created: String;
  password: String;
}

const userSchema = new Schema<IUser>({
  email: { validate: [isEmail,'invalid email'] ,type: String, required: true, unique: true },
  name: { type: String, required: true },
  address: { type: String, required: true },
  phone: { type: Number, required: true, unique: true },
  gender: { type: String, required: true },
  dob: { type: String, required: true },
  category: { type: String, required: true },
  created: { type: String, required: true },
  password: { type: String, required: true },
});

const User = model<IUser>("User", userSchema);

export { User };
